/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.Lista.Lista;
import Modelo.Persona;

/**
 *
 * @author Jose Yangari
 */
public class PersonaDao extends AdaptadorDao<Persona> {

    private Persona persona;
    

    public PersonaDao() {
        super(Persona.class);
    }
    
    public Persona getPersona() {
        if (persona == null) 
            persona = new Persona(); 
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public boolean guardar(){
        persona.setId(new Long(Listar().tamanio()+1));
        return guardar(persona);
    }
}
