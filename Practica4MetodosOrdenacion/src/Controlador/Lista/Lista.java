/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Lista;

import Modelo.Persona;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *
 * @author RODRIGO
 */
public class Lista<T> implements Serializable {

    private Nodo cabecera;
    private Class clazz;
    public static final Integer ASCENDENTE = 1;
    public static final Integer DESCENDENTE = 2;

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Lista() {
        this.cabecera = null;
    }

    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    public void imprimir() {

        if (!estaVacio()) {
            Nodo tmp = cabecera;//
            while (tmp != null) {
                System.out.println(tmp.getDato());
                tmp = tmp.getSiguiente();
            }
        }
    }

    private void insertar(T dato) {
        Nodo tmp = new Nodo(dato, cabecera);
        cabecera = tmp;
    }

    public boolean insertarDato(T dato) {
        try {
            inserTarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public T extraer() {
        T dato = null;
        if (!estaVacio()) {
            dato = (T) cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    public T consultarDatoPos(int pos) {
        T dato = null;
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            Nodo tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = (T) tmp.getDato();
            }
        }
        return dato;
    }

    public boolean modificarDatoPos(int pos, T data) {

        if (!estaVacio() && (pos <= tamanio() - 1)) {
            Nodo tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                tmp.setDato(data);
                return true;
            }
        }
        return false;
    }

    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            Nodo tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    public void insertar(T dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            Nodo iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            Nodo tmp = new Nodo(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);
        }
    }

    private void inserTarFinal(T dato) {
        insertar(dato, tamanio() - 1);
    }

    private Field getField(String nombre) {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.getName().equalsIgnoreCase(nombre)) {
                field.setAccessible(true);
                return field;
            }
        }
        return null;
    }

    /**
     * public void testReflect(T dato, String atributo) { try {
     * System.out.println(getField(atributo).get(dato).toString()); } catch
     * (Exception e) { System.out.println("Error"+e); } }*
     */
    private Object value(T dato, String atributo) throws Exception {
        return getField(atributo).get(dato);
    }

    //Ordenamiento por Metodo de seleccion
    public Lista<T> seleccion_clase(String atributo, Integer ordenacion) {
//        Lista<T> a=this;
        try {
            int i, j, k = 0;
            T t = null;
            int n = tamanio();

            for (i = 0; i < n - 1; i++) {
                k = i;
                t = consultarDatoPos(i);

                for (j = i + 1; j < n; j++) {
                    boolean band = false;
                    Object datoT = value(t, atributo);
                    Object datoj = value(consultarDatoPos(j), atributo);
                    if (datoT instanceof Number) {
                        Number aux = (Number) datoT;
                        Number numero = (Number) datoj;
                        band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? numero.doubleValue() < aux.doubleValue()
                                : numero.doubleValue() > aux.doubleValue();
                        /*if (numero.doubleValue() < aux.doubleValue()) {
                            t = a.consultarDatoPos(j);
                            k = j;
                        }*/
                    } else {
                        band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? datoT.toString().compareTo(datoj.toString()) > 0
                                : datoT.toString().compareTo(datoj.toString()) < 0;
                        /*if (datoT.toString().compareTo(datoj.toString()) > 0) {
                            t = a.consultarDatoPos(j);
                            k = j;
                        }*/
                    }
                    if (band) {
                        t = consultarDatoPos(j);
                        k = j;
                    }
                }
                modificarDatoPos(k, consultarDatoPos(i));
                modificarDatoPos(i, t);
            }
        } catch (Exception e) {
            System.out.println("Error en ordenacion" + e);
        }
        return this;
    }

    public Lista<T> shellClase(String atributo, Integer ordenacion) {
        try {
            int salto, i;
            T t = null;
            T a = null;
            boolean cambios;

            for (salto = tamanio() / 2; salto != 0; salto /= 2) {
                cambios = true;

                while (cambios) {   // Mientras se intercambie algún elemento                                         
                    cambios = false;
                    for (i = salto; i < tamanio(); i++) // se da una pasada
                    {
                        t = consultarDatoPos(i);
                        boolean band = false;
                        Object datoT = value(t, atributo);
                        Object datoj = value(consultarDatoPos(i - salto), atributo);
                        if (datoT instanceof Number) {
                            Number aux = (Number) datoT;
                            Number numero = (Number) datoj;
                            band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                    ? numero.doubleValue() > aux.doubleValue()
                                    : numero.doubleValue() < aux.doubleValue();
                        } else {
                            band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                    ? datoT.toString().compareTo(datoj.toString()) < 0
                                    : datoT.toString().compareTo(datoj.toString()) > 0;
                        }
                        if (band) {
//                            arreglo[i - salto] > arreglo[i]
                            a = consultarDatoPos(i);
                            modificarDatoPos(i, consultarDatoPos(i - salto));
                            modificarDatoPos(i - salto, t);
                            cambios = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error en ordenacion" + e);
        }
        return this;
    }

    public Lista<T> quickSort(String atributo, Integer ordenacion, Integer izq, Integer der) {
        try {
            int i, ult, m;
            T tmp = null;
            T t = null;
            if (izq >= der) {
                return this;
            }
            tmp = consultarDatoPos(izq);
            m = (izq + der) / 2;
            modificarDatoPos(izq, consultarDatoPos(m));
            modificarDatoPos(m, tmp);
            ult = izq;
            for (i = izq + 1; i <= der; i++) {
                t = consultarDatoPos(i);
                boolean band = false;
                Object datoT = value(tmp, atributo);
                Object datoj = value(t, atributo);
                if (datoj instanceof Number) {
                    Number aux = (Number) datoT;
                    Number numero = (Number) datoj;
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? numero.doubleValue() < aux.doubleValue()
                            : numero.doubleValue() > aux.doubleValue();
                } else {
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? datoT.toString().compareTo(datoj.toString()) > 0
                            : datoT.toString().compareTo(datoj.toString()) < 0;
                }
                
                if (band) {
                    tmp = consultarDatoPos(++ult);
                    modificarDatoPos(ult, consultarDatoPos(i));
                    modificarDatoPos(i, tmp);
                }
                tmp = consultarDatoPos(izq);
                modificarDatoPos(izq, consultarDatoPos(ult));
                modificarDatoPos(ult, tmp);

                quickSort(atributo, ordenacion, izq, ult - 1);
                quickSort(atributo, ordenacion, ult + 1, der);
            }
        } catch (Exception e) {
            System.out.println("Error a la ordenacion" + e);
        }
        return this;
    }

    public Lista<T> QuicksortLista2(String a, Integer ordenacion, Integer primero, Integer ultimo) {
        try {
            int i, j, central;
            T pivote = null;
            central = (primero + ultimo) / 2;
            pivote = consultarDatoPos(primero);
            Lista tmp = null;
            i = primero;
            j = ultimo;
            T p, p1, p2;
            do {
                p = consultarDatoPos(i);
                p1 = consultarDatoPos(j);
                p2 = pivote;
                boolean band = false;
                Object datoT = value(p, a);
                Object datoj = value(p2, a);
                Object datoi = value(p1, a);
                if (datoT instanceof Number) {
                    Number aux = (Number) datoT;
                    Number numero = (Number) datoj;
                    Number numero2 = (Number) datoi;
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? aux.doubleValue() <= numero.doubleValue()
                            : numero2.doubleValue() > numero.doubleValue();
                } else {
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? datoi.toString().compareTo(datoT.toString()) > 0
                            : datoi.toString().compareTo(datoj.toString()) < 0;
                }
//                while (pivote.toString().compareTo(consultarDatoPos(i).toString()) > 0) {
                while (band) {
//                    if (i < j) {
                    i++;
//                    }
                }
//                while (consultarDatoPos(j).toString().compareTo(pivote.toString()) > 0) {
                while (band) {
                    j--;
                }
                if (i <= j) {
                    T aux = consultarDatoPos(i);
                    modificarDatoPos(i, consultarDatoPos(j));
                    modificarDatoPos(j, aux);
//                    i++;
//                    j--;
                }
            } while (i < j);
            if (primero < j) {
                QuicksortLista2(a, ordenacion, primero, j - 1);
            }
            if (i + 1 < ultimo) {
                QuicksortLista2(a, ordenacion, j + 1, ultimo);
            }

        } catch (Exception e) {
        }

        return this;
    }

    public Lista<T> QuicksortLista(String a, Integer ordenacion, Integer primero, Integer ultimo) {
        try {

            Integer central = (primero + ultimo) / 2;
            T pivote = consultarDatoPos(central);
            int i = primero, j = ultimo;
            T auxiliar;
            T pi, pj;
            for (i = primero + 1; i <= ultimo; i++) {
                do {
                    System.out.println("1linea");
                    pi = consultarDatoPos(i);
                    pj = consultarDatoPos(j);
                    boolean band = false;
                    boolean band2 = false;
                    Object datoPivote = value(pivote, a);
                    Object datoi = value(pi, a);
                    Object datoj = value(pj, a);
                    System.out.println("Antes del datoj instanceof Number");
                    if (datoj instanceof Number) {
                        Number numeroPivote = (Number) datoPivote;
                        Number numeroi = (Number) datoi;
                        Number numeroj = (Number) datoj;
                        band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? numeroi.doubleValue() < numeroPivote.doubleValue()
                                : numeroi.doubleValue() > numeroPivote.doubleValue();

                        band2 = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? numeroj.doubleValue() < numeroPivote.doubleValue()
                                : numeroj.doubleValue() > numeroPivote.doubleValue();
                    } else {
                        band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? datoPivote.toString().compareTo(datoi.toString()) > 0
                                : datoPivote.toString().compareTo(datoi.toString()) < 0;

                        band2 = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? datoPivote.toString().compareTo(datoj.toString()) > 0
                                : datoPivote.toString().compareTo(datoj.toString()) < 0;
                    }

                    while (band) {
                        i++; // busca elemento mayor que pivote
                    }
                    while (band2) {
                        j--;           // busca elemento menor que pivote
                    }
                    if (i < j) {
                        auxiliar = consultarDatoPos(i);
                        modificarDatoPos(i, consultarDatoPos(j));
                        modificarDatoPos(j, auxiliar);
                        i++;
                        j++;
                    }
                    System.out.print("Si cumplio el while");
                } while (i < j);

//            modificarDatoPos(primero, consultarDatoPos(j));
//            modificarDatoPos(j, pivote);
                if (primero < j) {
                    QuicksortLista(a, ordenacion, primero, j);
                }
                if (i < ultimo) {
                    QuicksortLista(a, ordenacion, j, ultimo);
                }
            }
        } catch (Exception e) {
        }

        return this;
    }

}
