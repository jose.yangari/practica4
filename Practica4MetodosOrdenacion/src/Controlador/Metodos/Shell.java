/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Metodos;

import Controlador.Lista.Lista;

/**
 *
 * @author Jose Yangari
 */
public class Shell {

    public void shell(Integer[] arreglo) {

        int salto, aux, i;
        boolean cambios;

        for (salto = arreglo.length / 2; salto != 0; salto /= 2) {
            cambios = true;
            while (cambios) {   // Mientras se intercambie algún elemento                                         
                cambios = false;
                for (i = salto; i < arreglo.length; i++) // se da una pasada
                {
                    if (arreglo[i - salto] > arreglo[i]) {       // y si están desordenados
                        aux = arreglo[i];                  // se reordenan
                        arreglo[i] = arreglo[i - salto];
                        arreglo[i - salto] = aux;
                        cambios = true;              // y se marca como cambio.                                   
                    }
                }
            }
        }
    }

    public void shellLista(Lista a) {

        int salto, i;
        Object aux = null;
        Object t = null;
        boolean cambios;
        
        for (salto = a.tamanio() / 2; salto != 0; salto /= 2) {
            cambios = true;
            
            while (cambios) {   // Mientras se intercambie algún elemento                                         
                cambios = false;
                for (i = salto; i < a.tamanio(); i++) // se da una pasada
                {
                    t = a.consultarDatoPos(i);
                    if (t instanceof Number) {
                        Number numero = (Number) t;
                        Number numero2 = (Number) a.consultarDatoPos(i - salto);
                        if (numero2.doubleValue() > numero.doubleValue()) {       // y si están desordenados
                            aux = a.consultarDatoPos(i);                  // se reordenan
                            numero = numero2;
                            numero = (Number)aux;
                            cambios = true;              // y se marca como cambio.                                   
                        }
                    } else {
                        if (t.toString().compareTo(a.consultarDatoPos(i).toString()) > 0) {
                            String numero = (String) a.consultarDatoPos(i - salto);
                            String numero2 = (String) a.consultarDatoPos(i);
                            aux = a.consultarDatoPos(i);                  // se reordenan
                            numero2 = numero;
                            numero2 = aux.toString();
                            cambios = true;
                        }
                    }
                }
            }
        }
    }
}
