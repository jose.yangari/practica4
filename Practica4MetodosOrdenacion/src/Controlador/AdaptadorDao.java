/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.Lista.Lista;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Jose Yangari
 */
public class AdaptadorDao<T> implements InterfazDao<T>{
    
    private Class<T> clazz;
    private String capeta="datos"+File.separatorChar;//Carpeta para los datos
    private Lista<T> lista = new Lista<>();

    public AdaptadorDao(Class<T> clazz) {
        this.clazz = clazz;
        capeta += this.clazz.getSimpleName().toLowerCase()+".datos";
        lista.setClazz(clazz);
    }
    
    

    @Override
    public boolean guardar(T dato) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(capeta));
            Lista aux = Listar();
            aux.insertarDato(dato);
            oos.writeObject(aux);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error en Guardar"+e);
        }
        return false;
    }

    @Override
    public boolean modificar(T dato, int pos) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(capeta));
            Lista aux = Listar();
            aux.modificarDatoPos(pos, dato);
            oos.writeObject(aux);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error en Guardar");
        }
        return false;
    }

    @Override
    public Lista<T> Listar() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(capeta));
            lista = (Lista<T>)ois.readObject();
            ois.close();
        } catch (Exception e) {
        }
        return lista;
    }

    @Override
    public T buscarId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
