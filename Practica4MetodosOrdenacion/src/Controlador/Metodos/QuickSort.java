/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Metodos;

import Controlador.Lista.Lista;


/**
 *
 * @author Jose Yangari
 */
public class QuickSort {

    public void quickSort(Integer[] arreglo, int izq, int der) {

        //PIVOTE
        int pivote = arreglo[izq];
        //LOS ELEMENTOS > AL PIVOTE VAN A LA DERECHA
        //LOS ELEMENTOS < AL PIVOTE VAN A LA IZQUIERDA
        //VARIABLES AUXILIARES
        int i = izq, j = der, swap;

        //SE EVALUAN LOS ELEMENTOS PARA UBICAR EL NUEVO PIVOTE
        while (i < j) {
            //MIENTRAS QUE arreglo[i] SEA MENOR AL PIVOTE SE AUMENTA i
            //CUANDO SE CUMPLA EL WHILE EL ELEMENTO ES MAYOR AL PIVOTE
            //Y DEBERA IR A LA DERECHA
            while (arreglo[i] <= pivote && i < j) {
                i++;
            }
            //MIENTRAS QUE arreglo[j] SEA MAYOR AL PIVOTE SE DISMINUYE J
            //CUANDO SE CUMPLA EL WHILE EL ELEMENTO ES MENOR AL PIVOTE
            //Y DEBERA IR A LA IZQUIERDA
            while (arreglo[j] > pivote) {
                j--;
            }
            //SIEMPRE Y CUANDO i SEA MAYOR A j SE HACE UN INTERCAMBIO DE ELEMENTOS
            if (i < j) {
                swap = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = swap;
                //
            }
        }
        arreglo[izq] = arreglo[j];
        arreglo[j] = pivote;

        if (izq < j - 1) {
            quickSort(arreglo, izq, j - 1);
        }
        if (j + 1 < der) {
            quickSort(arreglo, j + 1, der);
        }
    }
    
    public static void quicksort(Integer A[], int izq, int der) {

        int pivote = A[izq]; // tomamos primer elemento como pivote
        int i = izq;         // i realiza la búsqueda de izquierda a derecha
        int j = der;         // j realiza la búsqueda de derecha a izquierda
        int aux;

        while (i < j) {                          // mientras no se crucen las búsquedas                                   
            while (A[i] <= pivote && i < j) {
                i++; // busca elemento mayor que pivote
            }
            while (A[j] > pivote) {
                j--;           // busca elemento menor que pivote
            }
            if (i < j) {                        // si no se han cruzado                      
                aux = A[i];                      // los intercambia
                A[i] = A[j];
                A[j] = aux;
            }
        }

        A[izq] = A[j];      // se coloca el pivote en su lugar de forma que tendremos                                    
        A[j] = pivote;      // los menores a su izquierda y los mayores a su derecha

        if (izq < j - 1) {
            quicksort(A, izq, j - 1);          // ordenamos subarray izquierdo
        }
        if (j + 1 < der) {
            quicksort(A, j + 1, der);          // ordenamos subarray derecho
        }
    }

    public void quickSort2(Integer[] arreglo, int izq, int der) {
        int i, ult, m, tmp;
        if (izq >= der) {
            return;
        }
        tmp = arreglo[izq];
        m = (izq + der) / 2;
        arreglo[izq] = arreglo[m];
        arreglo[m] = tmp;
        ult = izq;

        for (i = izq + 1; i <= der; i++) {
            if (arreglo[i] < arreglo[izq]) {
                tmp = arreglo[++ult];
                arreglo[ult] = arreglo[i];
                arreglo[i] = tmp;
            }
            tmp = arreglo[izq];
            arreglo[izq] = arreglo[ult];
            arreglo[ult] = tmp;
            quickSort2(arreglo, izq, ult - 1);
            quickSort2(arreglo, ult + 1, der);
        }
    }

    public void quickSortLista(Lista a, int izq, int der) {

        //PIVOTE
        Object t = null;
        Object swap = null;
        Object pivote = a.consultarDatoPos(izq);
        int i = izq, j = der;
        Number aux = (Number) pivote;
        Object n = a.consultarDatoPos(i);
        Object n2 = a.consultarDatoPos(j);
        while (i < j) {
            Number numero = (Number) a.consultarDatoPos(i);
            Number numero2 = (Number) a.consultarDatoPos(j);
            while (numero.doubleValue() <= aux.doubleValue()) {
                if (i < j) {
                    i++;
                }
            }
            while (numero2.doubleValue() > aux.doubleValue()) {
                j--;
            }
            if (i < j) {
                swap = n;
                n = n2;
                n2 = swap;
            }
        }
        pivote = n2;
        n2 = pivote;

        if (izq < j - 1) {
            quickSortLista(a, izq, j - 1);
        }
        if (j + 1 < der) {
            quickSortLista(a, j + 1, der);
        }
    }
    
}
